build:
	docker build -t registry.gitlab.com/d34d-shippy/shippy-user-api .
	docker push registry.gitlab.com/d34d-shippy/shippy-user-api

run:
	docker run --net="host" \
		-p 8080 \
		-e SERVICE_ADDRESS=:50051 \
		-e API_ADDRESS=:8080 \
		shippy-user-api

deploy:
	sed "s/{{ UPDATED_AT }}/$(shell date)/g" ./deployments/deployment.tmpl > ./deployments/deployment.yml
	kubectl replace -f ./deployments/deployment.yml
